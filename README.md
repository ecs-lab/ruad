# RUAD

Implementation of RUAD, based on the paper: https://www.sciencedirect.com/science/article/abs/pii/S0167739X2200406X
It is implemented on the publically available dataset, collected by the University of Bologna, and described in: https://www.nature.com/articles/s41597-023-02174-3